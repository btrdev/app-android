package nl.hashtagbtrooster.btrooster.timetable.period;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import nl.hashtagbtrooster.btrooster.GenericFunctions;
import nl.hashtagbtrooster.btrooster.R;

import nl.jochembroekhoff.btr.api.BtrApi;
import nl.jochembroekhoff.btr.api.dataopslag.IDataopslag;
import nl.jochembroekhoff.btr.api.rooster.ApiRooster;
import nl.jochembroekhoff.btr.api.rooster.container.roosterles.BasisLes;
import nl.jochembroekhoff.btr.api.rooster.deel.LaadRooster;
import nl.jochembroekhoff.btr.api.rooster.deel.resultaat.LaadRoosterResultaat;

public class PeriodInterface {
    public  String code;
    public  String type;
    public  int week;
    public  String location;

    private LaadRoosterResultaat lrr = null;
    private Gson gson = new GsonBuilder().create();

    private IDataopslag cacheContainer;

    public PeriodInterface(String code, String type, int week, String locatie, Context context) {
        this.code    = code;
        this.type    = type;
        this.week    = week;
        this.location = locatie;

        boolean isConnected = GenericFunctions.checkConnectionStatus(context);

        if (isConnected) {
            // Initialize the API by setting the timetable URL and adding the available locations
            ApiRooster.setRoosterUrl("http://rooster.calvijncollege.nl/");

            ApiRooster.LOCATIES.put("Goes", "Goes Klein Frankrijk");
            ApiRooster.LOCATIES.put("GoesNoordhoeklaan", "Goes Noordhoeklaan");
            ApiRooster.LOCATIES.put("GoesStationspark", "Goes Stationspark");
            ApiRooster.LOCATIES.put("KrabbendijkeAppelstraat", "Krabbendijke Appelstraat");
            ApiRooster.LOCATIES.put("KrabbendijkeKerkpolder", "Krabbendijke Kerkpolder");
            ApiRooster.LOCATIES.put("Tholen", "Tholen");

            doIndex();
        }

        //Initialize cache container
         cacheContainer= BtrApi.getPrefContainer(context.getResources().getString(R.string.datamanger_key_cache_timetable));
    }

    public LaadRoosterResultaat getTimetable(Context context) {
        boolean isConnected = GenericFunctions.checkConnectionStatus(context);

        if (isConnected) {
            Log.v("connection status", "connected");
            lrr = LaadRooster.laadRooster(
                    code,
                    type,
                    week,
                    location
            );

            boolean cachingSucceeded = cacheTimetable();
        } else {
            boolean cacheRetrievalSucceeded = loadFromCache();
        }

//        Log.v("timetable test2 subject", lrr.getRooster2d()[4][2].getRoosterTekst().get(0).getVelden().get(0).toString());
//        Log.v("timetable test2 teacher", lrr.getRooster2d()[4][2].getRoosterTekst().get(0).getVelden().get(1).toString());
//        Log.v("timetable test2 extra", lrr.getRooster2d()[4][2].getRoosterTekst().get(1).getVelden().get(0).toString());
//        Log.v("timetable test2 check", Integer.toString(lrr.getRooster2d()[4][2].getRoosterTekst().size()));
//        Log.v("timetable test", lrr.toString());

//        convertTimetable(context);

        return lrr;
    }

    private boolean loadFromCache() {
        String possibleCache = cacheContainer.get(getCacheKey(), "");
        if(possibleCache.length() > 0) {
            try{
                lrr = BtrApi.stringToSerializable(possibleCache, LaadRoosterResultaat.class, true);
                return true;
            } catch (ClassNotFoundException | IOException ex) {
                return false;
            }
        }
        return false;
    }

    private String getCacheKey() {
        return "tt-cached-" + location + "_" + type + "_" + code + "_" + "_wk" + week;
    }

    public boolean cacheTimetable() {
        try {
            String serialized = BtrApi.serializableToString(lrr, true);

            cacheContainer.set(getCacheKey(), serialized);

            return true;
        } catch (IOException e) {
            return false;
        }
    }

    /**
     * Calls ApiRooster to index the set locations
     */
    public void doIndex() {
        ApiRooster.indexeer();
    }
}
