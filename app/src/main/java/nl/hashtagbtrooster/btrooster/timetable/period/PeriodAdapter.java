package nl.hashtagbtrooster.btrooster.timetable.period;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import nl.hashtagbtrooster.btrooster.R;

public class PeriodAdapter extends ArrayAdapter<Period> {
    public PeriodAdapter(Context context, ArrayList<Period> periods) {
        super(context, 0, periods);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Period period = getItem(position);
        PeriodViewHolder holder = new PeriodViewHolder();

        Boolean textShouldBeInvisible = true;

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_period, parent, false);

            // Lookup view for data manipulation
            holder.subject = (TextView) convertView.findViewById(R.id.tv_subject);
            holder.teacher = (TextView) convertView.findViewById(R.id.tv_teacher);
            holder.fragment = (TextView) convertView.findViewById(R.id.tv_fragment);
            holder.tillFragment = (TextView) convertView.findViewById(R.id.tv_till_fragment);
            holder.extra = (TextView) convertView.findViewById(R.id.tv_extra);
            holder.location = (TextView) convertView.findViewById(R.id.tv_location);
            holder.time = (TextView) convertView.findViewById(R.id.tv_time);
            holder.divider = (TextView) convertView.findViewById(R.id.tv_divider);
            holder.ivTillFragment = (ImageView) convertView.findViewById(R.id.tv_till_fragment_circle);
            holder.ivFragment = (ImageView) convertView.findViewById(R.id.tv_fragment_circle);

            convertView.setTag(holder);
        } else {
            holder = (PeriodViewHolder) convertView.getTag();
        }

        // Populate the data into the template view using the data object
        holder.subject.setText(period.subject);
        holder.teacher.setText(period.teacher);
        holder.fragment.setText(Integer.toString(period.fragment));
        holder.tillFragment.setText(Integer.toString(period.tillFragment));
        holder.extra.setText(period.extra);
        holder.location.setText(period.location);
        holder.time.setText(period.time);

        if (period.fragment != period.tillFragment - 1) {
            holder.ivTillFragment.setVisibility(View.VISIBLE);
            holder.tillFragment.setVisibility(View.VISIBLE);
            holder.tillFragment.setText(Integer.toString(period.tillFragment));
        } else {
            holder.ivTillFragment.setVisibility(View.INVISIBLE);
            holder.tillFragment.setVisibility(View.INVISIBLE);
            holder.tillFragment.setText("");
        }

        if (period.changed) {
            holder.ivFragment.setImageResource(R.drawable.circle_fragment_changed);
        } else {
            holder.ivFragment.setImageResource(R.drawable.circle_fragment);
        }

        // Set correct text opacity
        holder.extra.setAlpha(0.54f);

        if (period.extra != "") {
            System.out.println("period.extra " + period.extra);
            holder.extra.setText(period.extra.toString());
            System.out.println("Text color: "  + holder.extra.getCurrentTextColor());
            textShouldBeInvisible = false;
            holder.extra.setTextColor(ContextCompat.getColor(getContext(),
                                      android.R.color.secondary_text_light));
            holder.extra.setVisibility(View.VISIBLE);
        } else {
            if (textShouldBeInvisible != true) {
                holder.extra.setVisibility(View.INVISIBLE);
            }
        }

        if (period.cancelled) {
            holder.extra.setTextColor(Color.RED);
            holder.extra.setText(R.string.period_cancelled);
            holder.extra.setVisibility(View.VISIBLE);
        } else {
            if (textShouldBeInvisible) {
                holder.extra.setVisibility(View.INVISIBLE);
            }
        }

        /* Check if the period is an exception: this means that only the subject (title) will be shown
         * And the rest except for time will be hidden
         */

        if (period.isException) {
            holder.extra.setVisibility(View.INVISIBLE);
            holder.location.setVisibility(View.INVISIBLE);
            holder.teacher.setVisibility(View.INVISIBLE);
            holder.divider.setVisibility(View.INVISIBLE);
        } else {
            holder.extra.setVisibility(View.VISIBLE);
            holder.location.setVisibility(View.VISIBLE);
            holder.teacher.setVisibility(View.VISIBLE);
            holder.divider.setVisibility(View.VISIBLE);
        }

        // Return the completed view to render on screen
        return convertView;
    }

    static class PeriodViewHolder {
        TextView subject;
        TextView teacher;
        TextView fragment;
        TextView tillFragment;
        TextView extra;
        TextView location;
        TextView time;
        TextView divider;
        ImageView ivTillFragment;
        ImageView ivFragment;
    }
}
