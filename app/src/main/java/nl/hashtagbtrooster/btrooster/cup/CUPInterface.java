package nl.hashtagbtrooster.btrooster.cup;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import nl.hashtagbtrooster.btrooster.impl.DatamanagerAndroid;
import nl.jochembroekhoff.btr.api.BtrApi;
import nl.jochembroekhoff.btr.api.cup.deel.LogIn;
import nl.jochembroekhoff.btr.api.cup.deel.resultaat.LogInResultaat;

import nl.jochembroekhoff.btr.api.cup.*;
import nl.jochembroekhoff.btr.api.cup.deel.ZoekNamen;
import nl.jochembroekhoff.btr.api.cup.deel.resultaat.ZoekNamenResultaat;

public class CUPInterface {
    public static Context context;
    public String user;
    public String pin;

    public CUPInterface(Context context) {
        this.context = context;
        Log.d("CONTEXT", context.toString());

        ApiCup.setCupUrl("http://ccgobb.cupweb6.nl");
        // Start Api in background
        ApiCup.start();
    }

    public LogInResultaat login(String user, String pin) {
        ApiCup.setGebruiker(user);
        ApiCup.setPincode(pin);

        LogInResultaat loginResult = LogIn.logIn();

        return loginResult;
    }

    public List<String> seekUsername(String letters) {
        letters = letters.trim();

        ZoekNamenResultaat znr = ZoekNamen.zoek(letters);
        List<String> resultList;

        if (znr.gelukt()) {
            Log.d("status seekUsername():", "successful");
            resultList = new ArrayList<>(znr.getResultaat());
        } else {
            Log.d("status seekUserName():", "unsuccessful");
            resultList = new ArrayList<>();
        }

        resultList = znr.getResultaat();
        System.out.println("results for seekUsername(\"" + letters + "\"): " + resultList.toString());

        return resultList;
    }
}
