package nl.hashtagbtrooster.btrooster.impl;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Map;

import nl.jochembroekhoff.btr.api.dataopslag.IDataopslag;

import static android.app.PendingIntent.getActivity;

public class DatamanagerAndroid extends IDataopslag{

    public static Context CONTEXT;
    private final SharedPreferences sharedPrefs;
    private final SharedPreferences.Editor sharedPrefsEdit;

    public DatamanagerAndroid(String containerName){
        super(containerName);

        sharedPrefs = CONTEXT.getSharedPreferences(containerName, Context.MODE_PRIVATE);
        sharedPrefsEdit = sharedPrefs.edit();
    }

    /**
     * Clears this container
     */
    @Override
    public void clear() {
        sharedPrefsEdit.clear();
        sharedPrefsEdit.commit();
    }

    /**
     * Get a string, default value ""
     * @param s key in container
     * @return value default to empty string
     */
    @Override
    public String get(String s) {
        return get(s,"");
    }

    /**
     * Get a string, default value specified
     * @param s key in container
     * @param s1 default value
     * @return value default to s1
     */
    @Override
    public String get(String s, String s1) {
        return sharedPrefs.getString(s, s1);
    }

    @Override
    public int getInt(String s) {
        return getInt(s,0);
    }

    @Override
    public int getInt(String s, int i) {
        return sharedPrefs.getInt(s,i);
    }

    /**
     * Set (/override) a string in the container
     * @param s key in container
     * @param s1 string value for key
     */
    @Override
    public void set(String s, String s1) {
        sharedPrefsEdit.putString(s,s1);
        sharedPrefsEdit.commit();
    }

    /**
     * Set (/override) an int in the container
     * @param s key in container
     * @param i int value for key
     */
    @Override
    public void setInt(String s, int i) {
        sharedPrefsEdit.putInt(s,i);
    }
}
