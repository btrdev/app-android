package nl.hashtagbtrooster.btrooster.timetable.period;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Period {
    public final String subject;
    public final String teacher;
    public final String location;
    public final String time;
    public final int fragment;
    public final int tillFragment;
    public final boolean cancelled;
    public final boolean changed;
    public final boolean isException;
    public final String extra;
}
