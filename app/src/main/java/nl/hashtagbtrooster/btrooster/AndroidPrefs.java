package nl.hashtagbtrooster.btrooster;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import nl.jochembroekhoff.btr.api.dataopslag.IPreferences;

public class AndroidPrefs implements IPreferences {
    private String containerName;
    private String prefix;
    private Context context;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor sharedPreferencesEditor;

    @Override
    public IPreferences nieuw(String containerName) {
        AndroidPrefs prefNew = new AndroidPrefs();
        prefNew.setContext(context);
        prefNew.start(containerName);
        return prefNew;
    }

    @Override
    public void start(String containerName) {
        this.containerName = containerName;
        genPrefix();
        sharedPreferences = context.getSharedPreferences(prefix, Context.MODE_PRIVATE);
        sharedPreferencesEditor = sharedPreferences.edit();
    }

    @Override
    public void clear() {
        try {
            sharedPreferencesEditor.clear();
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: Error handling!
        }
    }

    @Override
    public String getData(String key) {
        return sharedPreferences.getString(prefix + key, null);
    }

    @Override
    public String getData(String key, String defaultValue) {
        return sharedPreferences.getString(prefix + key, defaultValue);
    }

    @Override
    public int getDataInt(String key) {
        return sharedPreferences.getInt(prefix + key, 0);
    }

    @Override
    public int getDataInt(String key, int defaultValue) {
        return sharedPreferences.getInt(prefix, defaultValue);
    }

    @Override
    public void setData(String key, String value) {
        sharedPreferencesEditor.putString(prefix, value);
    }

    @Override
    public void setDataInt(String key, int value) {
        sharedPreferencesEditor.putInt(prefix, value);
    }

    public void genPrefix() { prefix = "IPrefs_" + containerName; }
    public void setContext(Context context) {
        Log.d("CONTEXT 2", context.toString());
        this.context = context;
    }
}
