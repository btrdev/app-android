package nl.hashtagbtrooster.btrooster;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import nl.hashtagbtrooster.btrooster.impl.DatamanagerAndroid;
import nl.jochembroekhoff.btr.api.BtrApi;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Context context = getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.preference_file_key),
                Context.MODE_PRIVATE
        );

        // Dummy variable
        String currentLoginCode = "Rutger Broekhoff";
        String currentSchool = "16204";

        //Initialize datamanager
        DatamanagerAndroid.CONTEXT = getApplicationContext();
        BtrApi.setDatamanager(DatamanagerAndroid.class);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.current_login_code), currentLoginCode);
        editor.putString(getString(R.string.current_school), currentSchool);
        editor.commit();


        // Dummy variable
        boolean isLoggedIn = false;
        if (isLoggedIn) {
            startBTRoosterActivity();
        } else {
            startLogin();
        }
    }

    protected void startLogin() {
        // Open LoginActivity
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    protected void startBTRoosterActivity() {
        // Open TimetableActivity
        Intent intent = new Intent(this, BTRoosterActivity.class);
        startActivity(intent);
        finish();
    }

}

