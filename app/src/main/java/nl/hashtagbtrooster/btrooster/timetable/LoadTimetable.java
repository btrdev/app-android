package nl.hashtagbtrooster.btrooster.timetable;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.util.ArrayList;

import nl.hashtagbtrooster.btrooster.timetable.period.Period;
import nl.hashtagbtrooster.btrooster.timetable.period.PeriodAdapter;
import nl.hashtagbtrooster.btrooster.timetable.period.PeriodInterface;
import nl.jochembroekhoff.btr.api.rooster.container.roosterles.BasisLes;
import nl.jochembroekhoff.btr.api.rooster.deel.resultaat.LaadRoosterResultaat;

public class LoadTimetable implements Runnable {
    private final Context context;
    private final TimetableFragment timetableFragment;

    private String code;
    private String type;
    private int week;
    private String location;

    private PeriodInterface periodInterface;
    public PeriodAdapter periodAdapter;

    @Override
    public void run() {
        loadTimetable();
        this.periodAdapter = getTimetable(context);
    }

    private void loadTimetable() {
        periodInterface = new PeriodInterface(
                code,
                type,
                week,
                location,
                context.getApplicationContext()
        );

        periodInterface.getTimetable(context.getApplicationContext());

        if (type.equals("s"))
            convertTimetableStudent(context.getApplicationContext());

        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(
                "btrooster_shared_preferences",
                Context.MODE_PRIVATE
        );

        String timetableResult = sharedPreferences.getString(
                "timetable",
                "Cannot get value from shared preferences"
        );

        Log.d("timetableResult", timetableResult);
    }

    PeriodAdapter getTimetable(Context context) {
        return convertTimetableStudent(context);
    }

     private PeriodAdapter convertTimetableStudent(Context context) {
        LaadRoosterResultaat lrr = periodInterface.getTimetable(context);
        BasisLes rooster2d[][] = lrr.getRooster2d();

        ArrayList<Period> periodArray = new ArrayList<>();
        final PeriodAdapter periodAdapter = new PeriodAdapter(context, periodArray);

        // Quick check
        if (rooster2d.length != 0) {
            int dayCount = 0;
            for (BasisLes day[] : rooster2d) {
                if (dayCount == 4) {
                    if (day != null) {
                        for (BasisLes period : day) {
                            if (period != null && !period.isLeeg()) {
                                Log.v("test period: ", period.getRoosterTekstVolledig());

                                String subject = period.getRoosterTekst().get(0).getVelden().get(0).toString();
                                String teacher = period.getRoosterTekst().get(0).getVelden().get(1).toString();
                                String location = period.getRoosterTekst().get(0).getVelden().get(2).toString();
                                String extra = "";
                                if (period.getRoosterTekst().size() > 1) {
                                    if (period.getRoosterTekst().get(0).getVelden().size() >= 1)
                                        extra += period.getRoosterTekst().get(1).getVelden().get(0).toString();
                                }

                                periodAdapter.add(new Period(
                                        subject,
                                        teacher,
                                        location,
                                        "",
                                        period.getLesUur(),
                                        period.getLesUur() + period.getLesSpan(),
                                        false,
                                        false,
                                        false,
                                        extra
                                ));
                            }
                        }
                    }
                }

                dayCount++;
            }
        }

         new Handler(Looper.getMainLooper()).post(new Runnable() {
             @Override
             public void run() {
                 timetableFragment.loadTimetableTest(periodAdapter);
             }
         });

        return periodAdapter;
    }

    LoadTimetable(Context context, TimetableFragment timetableFragment) {
        this.context = context;
        this.timetableFragment = timetableFragment;

        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(
                "btrooster_shared_preferences",
                Context.MODE_PRIVATE
        );

        this.code = sharedPreferences.getString("code", "Unknown");
        this.location = sharedPreferences.getString("location", "Unknown");
        this.week = sharedPreferences.getInt("week", 54); // Week 54 does not exist in a year. This means there is an error.
        this.type = sharedPreferences.getString("type", "Unknown");
    }
}
