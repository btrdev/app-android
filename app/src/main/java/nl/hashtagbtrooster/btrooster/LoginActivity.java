package nl.hashtagbtrooster.btrooster;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class LoginActivity extends AppCompatActivity {

    public void onStudentButtonClick(View view) {
        Intent intent = new Intent(this, BTRoosterActivity.class);

        startActivity(intent);
        finish();
    }

    public void onTeacherButtonClick(View view) {
        Intent intent = new Intent(this, BTRoosterActivity.class);

        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_main);

        Context context = getApplicationContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);

        boolean isLoggedIn = sharedPreferences.getBoolean("is_logged_in", false);
        if (isLoggedIn) {
            Intent intent = new Intent(this,BTRoosterActivity.class);

            startActivity(intent);
            finish();
        } else {
            sharedPreferences.edit().putBoolean("is_logged_in", true).commit();
        }
    }

    @Override
    protected void onStart() {
        mGoFullscreen();

        super.onStart();
    }

    @Override
    protected void onResume() {
        mGoFullscreen();

        super.onResume();
    }

    protected void mGoFullscreen() {
        // Make app fullscreen
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
              | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
              | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
              | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
              | View.SYSTEM_UI_FLAG_FULLSCREEN
              | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        );
    }

}
